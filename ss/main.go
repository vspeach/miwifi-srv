package main

import (
	"github.com/gofiber/fiber/v2"
	"log"
)

func main() {
	app := fiber.New()

	api := app.Group("/api")

	api.Get("/health", func(ctx *fiber.Ctx) error {
		return ctx.JSON(map[string]interface{}{
			"status": "OK",
		})
	})

	app.Get("/", func(ctx *fiber.Ctx) error {
		return ctx.JSON(map[string]interface{}{
			"config":   app.Config(),
			"handlers": app.Handler(),
			"headers":  ctx.GetReqHeaders(),
		})
	})

	log.Fatal(app.Listen(":9009"))
}
